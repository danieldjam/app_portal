require 'savon'

#require 'wsdl_validator'

#wsdl = WsdlValidator.new('http://10.1.11.161:8051/wsReport/MEX?wsdl')
#wsdl.valid? xml_message



client = Savon.client(
    :wsdl => 'http://10.1.11.161:8051/wsReport/MEX?wsdl',
    :endpoint => 'http://10.1.11.161:8051/wsReport/IwsReport',
    :namespace => 'http://www.totvs.com/',
    #:namespaces => {
     # 'xmlns:tns' =>'http://www.totvs.com/', 
     # 'xmlns:a' => 'http://schemas.microsoft.com/2009/WebFault'
    #},
    ssl_verify_mode: :none,
    :log => true,
    :pretty_print_xml => true,
    basic_auth:['4456','040113'],
    log_level: :debug, 
    :element_form_default => :qualified,
    raise_errors: true,
    #:snakecase => true,
    #namespaces:{
      #'xmlns:wsx' => 'http://schemas.xmlsoap.org/ws/2004/09/mex',
      #'xmlns:wsu' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
      #'xmlns:wsa10' => 'http://www.w3.org/2005/08/addressing',
      #'xmlns:wsp' => 'http://www.w3.org/ns/ws-policy',
      #'xmlns:wsap' => 'http://schemas.xmlsoap.org/ws/2004/08/addressing/policy',
      #'xmlns:msc' => 'http://schemas.microsoft.com/ws/2005/12/wsdl/contract',
      #'xmlns:soap12' => 'http://schemas.xmlsoap.org/wsdl/soap12/',
      #'xmlns:wsa' => 'http://schemas.xmlsoap.org/ws/2004/08/addressing',
      #'xmlns:wsam' => 'http://www.w3.org/2007/05/addressing/metadata',
      #'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema',
      #'xsd:schema' => 'http://www.totvs.com/Imports',
      #'xsd:import1' => 'http://schemas.microsoft.com/2003/10/Serialization/',
      #'xsd:import2' => 'http://schemas.datacontract.org/2004/07/System',
      #'xsd:import3' => 'http://schemas.datacontract.org/2004/07/System.Reflection',
      #'xsd:import4' => 'http://schemas.microsoft.com/2003/10/Serialization/Arrays'
      #'xmlns:soap' => 'http://schemas.xmlsoap.org/wsdl/soap/',
      #'xmlns:wsaw' => 'http://www.w3.org/2006/05/addressing/wsdl',
      #'xmlns:soapenc' => 'http://schemas.xmlsoap.org/soap/encoding/',
      #'xmlns:wsa10' => 'http://www.w3.org/2005/08/addressing'
      #'xmlns:soapenv' => 'http://schemas.xmlsoap.org/soap/envelope/',
      #'xmlns:tot' => 'http://www.totvs.com/IwsReport/GetReportInfo',
      #"xmlns:tot" => "http://www.totvs.com/",
      #"xmlns:a" => "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
       #2 => "http://schemas.microsoft.com/2003/10/Serialization/",
       #3 => "http://schemas.datacontract.org/2004/07/System",
       #4 => "http://schemas.datacontract.org/2004/07/System.Reflection",
      
    #}
  )
  
   #puts client.operations

  message = {
    "codColigada" => '1',
    "idReport" => '194', 
  }

    response = client.call(
        :get_report_info,
        message: message,
        'attributes' => { "xmlns" => "http://www.totvs.com/"},
        'headers' => {"username" => '4456',"password" => '040113'}
    )
    
    results = response.body[:get_report_info_response][:get_report_info_result]
    
    puts results