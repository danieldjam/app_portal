class PointsController < ApplicationController
  layout 'site'

   def index 
    
      if params[:commit].to_s == "Enviar"
      #puts "enviando valores"
       enviarDados(params[:data_ini],params[:data_fim]) 
        request[:data_ini] = params[:data_ini]
        request[:data_fim] = params[:data_fim]
      else
       puts "ERROU!" 
      end
   
   end 
  
  def enviarDados(data_ini,data_fim)
   
    client = Savon.client(
    :wsdl => 'http://10.1.11.161/TOTVSBusinessConnect/wsConsultaSQL.asmx?wsdl',
    :log => true,
    :pretty_print_xml => true,
    basic_auth:["4456","040113"],
    ssl_verify_mode: :none,
    log_level: :debug, 
  )
  message = {
    #"codSentenca" => 'BATIDASS',
    #"codColigada" => '0',
    #"codAplicacao" => 'Y',
    #"Usuario" =>'mestre',
    #"Senha" => '@Pintos2019',
    #"parameters" => 'COD_PESSOA=4062;DATA_INI=2019-03-01 00:00:00;DATA_FIM=2019-03-10 00:00:00;',
    
    "codSentenca" => 'NEWPORTAL01_03',
    "codColigada" => '1',
    "codAplicacao" => 'P',
    "Usuario" => session[:username],
    "Senha" => session[:password],
    'parameters' => "'COD_PESSOA="+session[:username]+";DATA_INI="+request[:data_ini]+";DATA_FIM="+request[:data_fim]+";",
}

response = client.call(
      :realizar_consulta_sql_auth,
      message: message,
  )
  
  result = response.body[:realizar_consulta_sql_auth_response][:realizar_consulta_sql_auth_result]
  
  doc = Nokogiri::Slop(result)
  
  @result = doc.NewDataSet.Resultado
 

  rescue Exception => e 
   
  e.message

  end
 
 def justificar
    
  if params[:commit].to_s == "Enviar"
    #puts "enviando valores"
     salvarDados(params[:chapa],params[:motivo],params[:tipoocorrencia]) 
      request[:chapa] = params[:chapa]
      request[:motivo] = params[:motivo] 
      request[:tipoocorrencia] = params[:tipoocorrencia] 
  else
     redirect_to points_index_path, flash: { error: 'Nao foi possivel enviar os dados, para salvar !'}
  end

 end
 
 def salvarDados(chapa,motivo,tipoocorrencia)
   
  @client = Savon.client(
    :wsdl => 'http://10.1.11.161/TOTVSBusinessConnect/wsDataServer.asmx?wsdl',
    # env_namespace: :soapenv,
    # element_form_default: :qualified,
    :log => true,
    :pretty_print_xml => true,
    basic_auth:["4456","040113"],
    ssl_verify_mode: :none,
    log_level: :debug,
    encoding: 'UTF-8',
    #:content_type => "text/xml;charset=UTF-8", 
  )

  message = {
    'DataServerName' => 'PTODATAJUSTIFICATIVAEXCECOES',
    'XML' => '<![CDATA[
      <NewDataSet>
        <AJUSTFUN>
         <CODCOLIGADA>1</CODCOLIGADA>
         <CHAPA>'+request[:chapa]+'</CHAPA>
         <DATA>2020-01-25T00:00:00</DATA>
         <INICIO>850</INICIO>
         <FIM>1125</FIM>
         <NUMHORASSTR>04:35</NUMHORASSTR>
         <INICIOSTR>14:10</INICIOSTR>
         <FIMSTR>18:45</FIMSTR>
         <TIPOOCORRENCIA>'+request[:tipoocorrencia]+'</TIPOOCORRENCIA>
         <JUSTIFICATIVA>'+request[:motivo]+'</JUSTIFICATIVA>
         <ATITUDE>0</ATITUDE>
         <NUMHORAS>275</NUMHORAS>
         <CAMPOCALCULADO/>
         <ATITUDESTR/>
        </AJUSTFUN>
       </NewDataSet>]]>',
    'Contexto' => 'CODCOLIGADA=1;CODSISTEMA=G;CODUSUARIO=mestre;',
    'Usuario' => '4456',
    'Senha' => '040113',
  }  

  response = @client.call(
    :save_record_auth,
    message: message,
  )

  result = response.body[:save_record_auth_response][:save_record_auth_result]
  
  puts result

  #doc = Nokogiri::Slop(result)
  
  #@result = doc.NewDataSet
 
  rescue Exception => e 
   
  e.message 

 end

end