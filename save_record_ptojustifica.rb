require 'savon'


  @client = Savon.client(
    :wsdl => 'http://10.1.11.161/TOTVSBusinessConnect/wsDataServer.asmx?wsdl',
    #:endpoint => 'http://www.totvs.com.br/br/',
    #:namespace => "xmlns:br=http://www.totvs.com.br/br/",
    headers: {
      "Authorization" => 'Basic NDQ1NjowNDAxMTM=', 
      "Connection" => 'Keep-Alive', 
      "Accept-Encoding" => "gzip,deflate", 
      "Content-Type" => "text/xml;charset=utf-8",
    },
    #env_namespace: :soapenv,
    element_form_default: :qualified,
    :log => true,
    :pretty_print_xml => true,
    basic_auth:["4456","040113"],
    ssl_verify_mode: :none,
    log_level: :debug
  )

  puts @client.operations

  message = {
    'DataServerName'=>'PTODATAJUSTIFICATIVAEXCECOES',
    'XML'=>"<![CDATA[<NewDataSet><AJUSTFUN>
             <CODCOLIGADA>1</CODCOLIGADA>
             <CHAPA>012519</CHAPA>
             <DATA>2020-01-25T00:00:00</DATA>
             <INICIO>850/INICIO>
             <FIM>1125</FIM>
             <NUMHORASSTR>04:35</NUMHORASSTR>
             <INICIOSTR>14:10</INICIOSTR>
             <FIMSTR>18:45</FIMSTR>
             <TIPOOCORRENCIA>E</TIPOOCORRENCIA>
             <JUSTIFICATIVA> TESTANDO_A_TESTA </JUSTIFICATIVA>
             <ATITUDE>0</ATITUDE>
             <NUMHORAS>275</NUMHORAS>
             <CAMPOCALCULADO />
             <ATITUDESTR />
         </AJUSTFUN></NewDataSet>]]>",
    'Contexto' => 'CODSISTEMA=A;CODCOLIGADA=1;CODUSUARIO=4456',
    'Usuario' => '4456',
    'Senha' => '040113',
  }  

  response = @client.call(
    :save_record_auth,
    message: message,
  )

  result = response.body[:save_record_auth_response][:save_record_auth_result]

  puts result.to_s