require 'savon'

client = Savon.client(
    :wsdl => 'http://10.1.11.161/TOTVSBusinessConnect/wsConsultaSQL.asmx?wsdl',
    # env_namespace: :soapenv,
    # element_form_default: :qualified,
    :log => true,
    #:pretty_print_xml => true,
    basic_auth:["4456","040113"],
    ssl_verify_mode: :none,
    log_level: :debug, 
  )

#   client.operations

  message = {
    #"codSentenca" => 'BATIDASS',
    #"codColigada" => '0',
    #"codAplicacao" => 'Y',
    #"Usuario" =>'mestre',
    #"Senha" => '@Pintos2019',
    #"parameters" => 'COD_PESSOA=4062;DATA_INI=2019-03-01 00:00:00;DATA_FIM=2019-03-10 00:00:00;',
    
    "codSentenca" => 'TESTE_01_01',  #Pega PFUNCxPPESSOA
    "codColigada" => '1',
    "codAplicacao" => 'A',
    "Usuario" =>'mestre',
    "Senha" => '@Pintos2019',
    'parameters' => 'COD_PESSOA=4031;CODSITUACAO != D;',
}

response = client.call(
    :realizar_consulta_sql_auth,
    message: message,
)

results = response.body[:realizar_consulta_sql_auth_response][:realizar_consulta_sql_auth_result]

puts results