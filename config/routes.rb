Rails.application.routes.draw do
 
  #Rota Money Finalizadas 
  get 'moneys_exe/index'
  post 'moneys_exe/index'
  get '/export' => 'moneys_exe#export'
  
  #Historico de Justificativas
  
  get 'points_exe/index'
  post 'points_exe/index'
  

  #ROTA HISTORICO SALARIAL 
  get 'hist_moneys/index'
  post 'hist_moneys/index'

  #ROTA PONTO
  get 'points/index'
  post 'points/index'
  post 'points/justificar' => 'points#justificar'
  #post 'points/enviarTwo' => 'points#enviarTwo'
  get  'points/salvarDados'

  #Rota Money
  get 'moneys/index'
  post 'moneys/index'
  get '/money_export' => 'moneys#export'
  
  #exportando_pfd
  #get 'moneys/export'
   
  #Rota Dados Funcionários
  get 'dice/index'

  #Rota Página Principal
  get 'pages/index'
  delete 'pages/destroy'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  #Rotas login
  get 'login/index'
  root 'login#index'

end
