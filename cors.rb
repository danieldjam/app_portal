Rails.application.config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins '*' #http://www.totvs.com/

      resource '*', 
       :headers => :any, 
       :expose => ['access-token','expiry','token-type','uid','client'],
       :methods => [:get, :post, :options, :delete,:put]
    end
  end