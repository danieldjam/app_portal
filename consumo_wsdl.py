from zeep import Client

client = Client(wsdl = 'http://10.1.11.161/TOTVSBusinessConnect/wsDataServer.asmx?wsdl')

with client.settings(raw_response=True):
    response = client.service.myoperation()


    assert response.status_code == 200
    assert response.content