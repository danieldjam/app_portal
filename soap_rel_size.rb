require 'savon'
require 'base64'

client = Savon.client(
    :wsdl => 'http://10.1.11.161:8051/wsReport/MEX?wsdl',
    :endpoint => 'http://10.1.11.161:8051/wsReport/IwsReport',
    :namespace => 'http://www.totvs.com/',
    basic_auth:["4456","040113"],
    headers: {
      "Authorization" => 'Basic NDQ1NjowNDAxMTM=', 
      "Connection" => 'Keep-Alive' , 
      "Accept-Encoding" => "gzip,deflate", 
      "Content-Type" => "text/xml; charset=utf-8",
    },
    element_form_default: :qualified,
    :log => true,
    :pretty_print_xml => true,
    ssl_verify_mode: :none,
    log_level: :debug, 
    :raise_errors => true,
  )

   #client.operations

  message = {
    'guid' => '125d7b5a-d4fe-43fc-af55-a3800fd00bd5',
}

response = client.call(
    :get_generated_report_size,
    message: message
    
)

#p response.soap_fault?

results = response.body[:get_generated_report_size_response][:get_generated_report_size_result]


#.Base64.encode64(results)

puts results